<?php
/**
 * @file
 * ShortCode for embedding videos.
 */

/**
 * Implements hook_shortcode_info().
 */
function shortcode_video_shortcode_info() {
  $shortcodes['video'] = array(
    'title' => t('Video macro'),
    'description' => t('Inserts embedded video code.'),
    'process callback' => 'shortcode_video_macro_process',
    'attributes callback' => "shortcode_video_attributes",
    'tips callback' => 'shortcode_video_tip',
  );
  return $shortcodes;
}

/**
 * Implements hook_theme().
 */
function shortcode_video_theme() {
  return array(
    'shortcode_video_embed_error' => array(
      'variables' => array(
        'video_url' => '',
        'class' => 'video-embedding-error',
      ),
    ),
    'shortcode_video_embed_no_provider' => array(
      'variables' => array(
        'video_url' => '',
        'class' => 'video-embedding-no-provider',
      ),
    ),
    'shortcode_video_embed_youtube' => array(
      'variables' => array(
        'video_url' => '',
        'attributes' => array(),
      ),
    ),
    'shortcode_video_embed_vimeo' => array(
      'variables' => array(
        'video_id' => '',
        'attributes' => array(),
      ),
    ),
  );
}

/**
 * Attributes form element for video ShortCode.
 *
 * Used by ShortCode_wysiwyg module to add form elements for attributes.
 */
function shortcode_video_attributes($form, &$form_state) {
  $form['video-height'] = array(
    '#title' => t('Height'),
    '#type' => 'textfield',
    '#states' => array(
      'visible' => array(
        ':input[name="shortcode"]' => array('value' => 'video'),
      ),
    ),
  );
  $form['video-width'] = array(
    '#title' => t('Width'),
    '#type' => 'textfield',
    '#states' => array(
      'visible' => array(
        ':input[name="shortcode"]' => array('value' => 'video'),
      ),
    ),
  );
  $form['video-class'] = array(
    '#title' => t('class'),
    '#type' => 'textfield',
    '#states' => array(
      'visible' => array(
        ':input[name="shortcode"]' => array('value' => 'video'),
      ),
    ),
  );
  return $form;
}

/**
 * Provides Youtube video embedding ShortCode macro.
 *
 * <iframe width="420" height="315" title="Foo"
 * src="http://www.youtube.com/embed/EJu8ihVdygY?rel=0"
 * frameborder="0" allowfullscreen></iframe>
 */
function shortcode_video_macro_process($attrs, $text) {
  $attrs = shortcode_attrs(
    array(
      'class' => '',
      'width' => '',
      'height' => '',
      'showlink' => FALSE,
      'sub' => '',
      'forcesub' => FALSE,
      'title' => '',
    ),
    $attrs
  );

  $text = decode_entities($text);
  $url = drupal_parse_url($text);

  // Custom url parser regexp:
  // preg_match('!^.*\:\/\/(www\.)?([^\.]*)(\.)([^\/\?]*)([\/]?)([^\?\"]*)\??([^\"\>]?.*)?$!', $text, $m);

  // Get provider.
  $m = [];
  preg_match('!^.*\:\/\/(www\.)?([^\?\/]*)(.*)$!', $url['path'], $m);
  $provider = $m[2];

  $output = '';
  switch ($provider) {
    case 'youtu.be':
    case 'youtube.com':
      if (!empty($url['query']['v'])) {
        $params['video_id'] = $url['query']['v'];
        unset($params['v']);
        unset($url['query']['v']);
      }
      else {
        $id = trim($m[3], '/');
        switch ($id) {
          case 'watch':
            // todo: try special parsing.
            // $params = _shortcode_video_parse_params($m);
            break;

          default:
            $params['video_id'] = $id;
            break;
        }
      }

      $embed_url = '//www.youtube.com/embed/';
      $params['attributes'] = $attrs;

      if (!empty($params['video_id'])) {
        if ($attrs['sub']) {
          $url['query']['hl'] = $attrs['sub'];
        }
        if ($attrs['forcesub']) {
          $url['query']['fs'] = 1;
        }
        if (!empty($url['query']['t'])) {
          $t = explode('m', $url['query']['t']);
          $start = intval(trim($t[0])) * 60;
          $start += intval($t[1]);

          unset($url['query']['t']);
          $url['query']['start'] = $start;
        }
        $url['external'] = TRUE;
        $params['video_url'] = url($embed_url . $params['video_id'], $url);
        $output = theme('shortcode_video_embed_youtube', $params);
      }
      break;

    case 'vimeo.com':
      $id = trim($m[3], '/');
      $params['video_id'] = $id;
      $embed_url = '//player.vimeo.com/video/';
      $params['external'] = TRUE;
      $params['video_url'] = url($embed_url . $params['video_id'], $params);
      if (!empty($params['video_id'])) {
        $params['attributes'] = $attrs;
        $output = theme('shortcode_video_embed_vimeo', $params);
      }
      break;

    default:
      // Alter the output.
      // $context1 = $text.
      // $context2 = $attrs - the attributes array
      // $context3 = $url - The parsed URL by Drupal.
      drupal_alter('shortcode_info', $output, $text, $attrs, $url);

      if (empty($output)) {
        // Default behaviour when no custom provider.
        $pattern = array(
          '!http://!', '!https://!',
        );
        $text = preg_replace($pattern, '//', $text);
        $output = theme('shortcode_video_embed_no_provider',
          array('video_url' => $text)
        );
      }
      break;
  }

  return $output;
}

/**
 * Provides tip callback for tip ShortCode.
 */
function shortcode_video_tip($format, $long) {
  $output = array();
  $output[] = '<p><strong>' . t('[video]video url[/video]') . '</strong> ';
  if ($long) {
    $output[] = t('Embeds the video into the text.') . '</p>';
  }
  else {
    $output[] = t('Embeds video into the content text. With the <em>width</em> and <em>height</em> parameter you can specify the video size. Use <em>title</em> to specify a title attribute.') . '</p>';
    $output[] = '<p>' . t('For YouTube videos you can specify the width with 420, 480, 640, 960 as the default videos sizes, then the height of the video will be added according to the default embed sizes. The default video size is 480x360px.') . '</p>';
  }

  return implode('', $output);
}

/*
 * Theme functions
 */

/**
 * Provides video embed error theme function.
 */
function theme_shortcode_video_embed_error($variables) {
  return '<span class="' . $variables['class'] . '">' . check_plain($variables['video_url']) . '</span>';
}

/**
 * Provides video no provider theme function.
 */
function theme_shortcode_video_embed_no_provider($variables) {
  return '<span class="' . $variables['class'] . '">' . check_plain($variables['video_url']) . '</span>';
}

/**
 * Provides YouTube video embed theme function.
 */
function theme_shortcode_video_embed_youtube($variables) {
  $attrs = $variables['attributes'];

  $height = empty($attrs['height']) ? 0 : intval($attrs['height']);
  $width = empty($attrs['width']) ? 0 : intval($attrs['width']);

  // Set defaults.
  if (empty($width) && empty($height)) {
    $width = 480;
    $height = 360;
  }
  elseif ($width && empty($height)) {
    switch ($width) {
      case 450:
        $height = 315;
        break;

      case 480:
        $height = 360;
        break;

      case 640:
        $height = 480;
        break;

      case 960:
        $height = 720;
        break;

      default:
        $height = intval($width * 0.75);
        break;

    }
  }
  else {
    $width = intval($height * 1.33);
  }

  $width = $width ? ' width="' . $width . '"' : '';
  $height = $height ? ' height="' . $height . '"' : '';
  $title = empty($attrs['title']) ? '' : ' title="' . check_plain($attrs['title']) . '"';

  $out = '<div class="video-container">';
  $out .= '<iframe' . $width . $height . $title . ' src="' . $variables['video_url'] . '" frameborder="0" allowfullscreen></iframe>';
  $out .= '</div>';

  return $out;
}

/**
 * Provides Vimeo video embed theme function.
 */
function theme_shortcode_video_embed_vimeo($variables) {
  $attrs = $variables['attributes'];

  $height = empty($attrs['height']) ? 500 : intval($attrs['height']);
  $width = empty($attrs['width']) ? 281 : intval($attrs['width']);
  $title = empty($attrs['title']) ? '' : check_plain($attrs['title']);

  // Set defaults.
  if (empty($width) && empty($height)) {
    $width = 500;
    $height = 281;
  }
  elseif ($width && empty($height)) {
    $height = intval($width * 0.75);
  }
  else {
    $width = intval($height * 1.33);
  }

  $width = $width ? ' width="' . $width . '"' : '';
  $height = $height ? ' height="' . $height . '"' : '';
  $title = empty($attrs['title']) ? '' : ' title="' . check_plain($attrs['title']) . '"';

  $out = '<div class="video-container">';
  $out .= '<iframe src="' . $variables['video_url'] . '" ' . $width . $height . $title . ' frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
  $out .= '</div>';

  return $out;
}
